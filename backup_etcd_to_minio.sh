#!/bin/bash
# 获取当前时间
now=$(date +%Y%m%d%H%M)
scheme="http://"
if [ "${USE_SECURE_CONN_TO_MINIO}" = "True" ]; then scheme="https://"; fi

ETCDCTL_API=3 etcdctl --endpoints=${ETCD_SERVERS} \
    --cacert=${ETCD_CAFILE} \
    --cert=${ETCD_CERTFILE} \
    --key=${ETCD_KEYFILE} \
    snapshot save "etcd-${now}"
if [ $? -ne 0 ]; then
    echo "failed to create snapshot"
    exit 1
fi
echo "create snapshot successfully"

mc alias set local "${scheme}${MINIO_SERVER_URL}" "${MINIO_ACCESS_KEY}" "${MINIO_SECRET_KEY}"
mc cp "etcd-${now}" "local/${MINIO_BUCKET_NAME}/etcd-${now}"
if [ $? -ne 0 ]; then
    echo "failed to upload snapshot file to minio"
    exit 1
fi
echo "upload snapshot file to minio successfully"
