FROM python:3.11.6-alpine
WORKDIR /app
RUN pip config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple
COPY --from=bitnami/etcd:3.5.7 /opt/bitnami/etcd/bin/etcdctl /usr/local/bin/
COPY requirements.txt ./
RUN pip3 install -r requirements.txt
COPY *.py ./
CMD ["/usr/local/bin/python3", "backup_etcd_to_minio.py"]
