import os
import time
from minio import Minio
from minio.error import S3Error

# 获取当前时间
now = time.strftime("%Y%m%d%H%M", time.localtime())
print(now)
# etcd服务器的地址
etcd_url = os.getenv("ETCD_SERVERS")
print(etcd_url)
# 验证etcd证书所用的CA
cacert = os.getenv("ETCD_CAFILE")
print(type(cacert))
print(cacert)
# 访问etcd服务使用的证书和私钥
cert = os.getenv("ETCD_CERTFILE")
print(cert)
key = os.getenv("ETCD_KEYFILE")
print(key)
# minio server的地址，以“域名或ip:端口的形式给出”，如“minio.liguanghui.pro:9000”
minio_server_url = os.getenv("MINIO_SERVER_URL")
print(minio_server_url)

use_secure_conn_to_minio = False
if os.getenv("USE_SECURE_CONN_TO_MINIO") == "True":
    use_secure_conn_to_minio = True
print(use_secure_conn_to_minio)

backup_file_name = "etcd-" + now
print(backup_file_name)

# 访问Minio服务使用的户名和密码
minio_access_key = os.getenv("MINIO_ACCESS_KEY"),
minio_secret_key = os.getenv("MINIO_SECRET_KEY"),
bucket_name = os.getenv("MINIO_BUCKET_NAME")


def create_snapshot():
    command = "ETCDCTL_API=3 etcdctl --endpoints=" + etcd_url + " --cacert=" + cacert + " --cert=" + cert + " --key=" + key + " snapshot save " + backup_file_name
    os.system(command=command)


def upload_to_minio():
    client = Minio(
        endpoint=minio_server_url,
        access_key=str(minio_access_key),
        secret_key=str(minio_secret_key),
        secure=use_secure_conn_to_minio
    )
    found = client.bucket_exists(bucket_name)
    # if not found:
    #     client.make_bucket(bucket_name)
    #     print(bucket_name + " bucket created")
    # else:
    #     pass
    if not found:
        print("bucket " + bucket_name + " does not exists, please create it first")
        exit(0)
    client.fput_object(bucket_name, backup_file_name, backup_file_name)


if __name__ == "__main__":
    try:
        create_snapshot()
        print("create snapshot successfully")
    except Exception as e:
        print(e)
    try:
        upload_to_minio()
        print("upload snapshot file to minio successfully")
    except S3Error as e:
        print("upload failed: " + str(e))
